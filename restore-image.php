<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>DRBL WebUI</title>
</head>

<body>
<div id ="banner">
    <a href="index.php"><img src = "logo.png" class = "centered" /></a>
    <h2 class="centered">App Academy DRBL WebUI</h2>
</div> <!-- banner -->
    
<?php
    require_once('functions/functions.php');
    
    $numberToClone = filter_input(INPUT_POST, 'numberToClone');
    $imageToClone = filter_input(INPUT_POST, 'imageToClone');
    
    /** In the BASH script, restore_image.sh:
        $1 = numberToClone
        $2 = imageToClone **/
    
    $cmd = 'sudo scripts/restore_image.sh ' . $numberToClone . ' ' . $imageToClone;
?>
    
    <div class="wrapper" class="centered">    
        <div id="prepBox" class="actionBox">
            <fieldset>
                <legend>Preparations:</legend>
                <p>Number to clone is: <?php echo $numberToClone; ?></p>
                <p>Image to clone is: <?php echo $imageToClone; ?></p>
                <p>We'll be trying this command: <code><?php echo $cmd; ?></code></p>
            </fieldset>
        </div> <!-- end prepBox -->
        <br />
        <div id="outputBox" class="actionBox">
            <fieldset>
                <legend>Restore Script Output:</legend>
                <?php
                    /** exec_with_output is a function in functions/functions.php
                        which allows executing shell commands and reading the
                        output in real time **/
                    exec_with_output($cmd);
                    echo '<a class="pushLeft" href="logs/save-attempt.log">[View the Log]</a>';
                    echo '<a class="pushRight" href="index.php">[Return to Main Screen]</a>';
                ?>
            </fieldset>
        </div> <!-- end outputBox -->
    </div> <!-- end wrapper -->
</body>
</html>