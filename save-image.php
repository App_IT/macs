<html>
<head>    
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>DRBL WebUI</title>
</head>
    
<body>
<div id="banner">
    <a href="index.php"><img src="logo.png" class="centered" /></a>
    <h2 class="centered">App Academy DRBL WebUI</h2>
</div> <!-- banner -->
    
    <?php
    require_once('functions/functions.php');
    $imageName = filter_input(INPUT_POST, 'imageName');
    
    $imageName = preg_replace('/\s+/', '_', $imageName);
    preg_match('/^(\w|\-|\.)+$/', $imageName, $match);
    
    if ($match) {
        $filenameOK = true;
    } else {
        $filenameOK = false;
    }
    
    /** In the BASH script, save_image.sh:
        $1 = imageName (name of image to be saved)
    **/
    
    $cmd = 'sudo scripts/save_image.sh ' . $imageName;
    ?>

    <div class="wrapper" class="centered">    
        <div id="prepBox" class="actionBox">
            <fieldset>
                <legend>Preparations:</legend>
                <?php
                if ($filenameOK) {
                    echo
                    '<p>Filename is valid.</p>
                     <p>Image will be saved as: ' . $imageName . '</p>
                     <p>We\'ll be trying this command: <code>' . $cmd . '</code></p>';
                } else {
                    echo
                    '<p class="error">This filename is invalid: <code>' . $imageName . '</code></p>
                     <p class="error">It can only contain:<br />
                     [alphabet characters], [numeric digits], [hypens: - ], [underscores: _ ] [and dots: . ]</p>';
                }
                ?>    

            </fieldset>
        </div> <!-- end prepBox -->
        <br />
        <div id="outputBox" class="actionBox">
            <fieldset>
                <legend>Save Script Output:</legend>
                <?php
                    if ($filenameOK) {
                        /** exec_with_output is a function in functions/functions.php
                            which allows executing shell commands and reading the
                            output in real time **/
                        exec_with_output($cmd);
                        echo '<br />';
                        echo '<a class="pushLeft" href="logs/save-attempt.log">[View the Log]</a>';
                        echo '<a class="pushRight" href="index.php">[Return to Main Screen]</a>';
                    } else {
                        echo '<p class="error">An error was encountered before the script was run</p>';
                        echo '<a class="centered" href="index.php">[Return to Main Screen]</a>';
                    }
                ?>
            </fieldset>
        </div> <!-- end outputBox -->
    </div> <!-- end wrapper -->
</body>
</html>