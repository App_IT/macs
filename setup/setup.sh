#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "Please run this script as root"
    exit 1
else
    echo "We are root. (yay!)"
fi

release=`lsb_release -sc`

echo "Getting the GPG Key for DRBL..."
wget http://drbl.nchc.org.tw/GPG-KEY-DRBL

echo "Adding the GPG Key for DRBL..."
apt-key add GPG-KEY-DRBL
rm GPG-KEY-DRBL*

echo "Checking for DRBL repo in /etc/apt/sources.list..."
check_repo=`grep 'free.nchc.org' /etc/apt/sources.list`

if [ -z $check_repo ]; then
  echo "DRBL Repo was not found.  Adding it."
  echo "Updating Source List"
  echo "deb http://free.nchc.org.tw/ubuntu $release main restricted universe multiverse" >> /etc/apt/sources.list
  echo "deb http://free.nchc.org.tw/drbl-core drbl stable" >> /etc/apt/sources.list
else
  echo "DRBL Repo was found in /etc/apt/sources.list.  Not re-adding it."
fi


echo "Updating Available Repositories..."
apt-get update

echo "Installing DRBL (Clonezilla Server)"
apt-get -y install drbl

echo "Set up the server DRBL..."
drblsrv -i

echo "Configure DRBL..."
drblpush -i

echo "DRBL Setup Complete!"
echo ""
echo ""

echo "Checking if WWW_USER alias is already set up"
check_root_alias=`grep 'WWW_USER' /etc/sudoers`

if [ "$check_root_alias" == "" ]; then
  echo "WWW_USER alias not found in /etc/sudoers.  Adding it..."
  echo "Setting up root access for Web UI Scripts..."
  echo "User_Alias WWW_USER = www-data" >> /etc/sudoers
  echo "Cmnd_Alias WWW_COMMANDS = /var/www/html/macs/scripts/restore_image.sh, /var/www/html/macs/scripts/save_image.sh" >> /etc/sudoers
  echo "WWW_USER ALL=(ALL) NOPASSWD: WWW_COMMANDS" >> /etc/sudoers
else
  echo "WWW_USER alias was found in /etc/sudoers.  Skipping this step."
fi

echo "All Done!!"
exit 0
