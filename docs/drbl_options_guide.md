##### App Academy MaCS Installation: DRBL Options Guide

This guide is meant to demonstrate the options that App Academy uses for DRBL installation.  Your situation may be different, but these options work with the Dell Latitude 3330 Laptop which App Academy uses as a cloning server at the time of the creation of this document.  Most notably, your eth0 and wlan0 devices may be named differently.  You can find your network adapters names by typing ifconfig in a terminal.
This guide assumes you have run the following command:

`tech@cloner:/var/www/html/drbl_gui/setup$ sudo ./setup.sh`

Below are the options App Academy uses when installing DRBL.  If an option not listed or youre unsure what to use, use the default option (just press enter to accept the default).

Do you want to install the network installation boot images so that you can let the client computer install some GNU/Linux distributions (Debian, Ubuntu, RedHat Linux, Fedora Core, Mandriva, CentOS and OpenSuSE...) via a network connection?  !!NOTE!! This will download a lot of files (Typically > 100 MB) so it might take a few minutes. If the client computer has a hard drive that you may install GNU/Linux onto, put a Y here. If you answer "no" here, you can run "drbl-netinstall" to install them later.
[y/N] N

Do you want to use the serial console output on the client computer(s)?
If you do NOT know what to pick, say "N" here, otherwise the client computer(s) may show NOTHING on the screen!
[y/N] N

There are 2 kernels available for clients, which one do you prefer?
[1]: kernel 4.4.0-21-generic x86_64 (from this DRBL server)
[2]: linux-image-4.4.0-38-generic (from APT repository)
[1] 1
Please enter DNS domain (such as drbl.sf.net):
[drbl.org] <<type anything or nothing>>
Set DOMAIN as drbl.org
------------------------------------------------------
Please enter NIS/YP domain name:
[penguinzilla] <<type anything or nothing>>
This prefix is used to automatically create hostname for clients. If you want to overwrite some or all automatically created hostnames, press Ctrl-C to quit this program now, edit /etc/drbl/client-ip-hostname, then run this program again.
[tech-box-] <<type whatever you want the hostname (computer name) prefix to be>>
Set the client hostname prefix as <<what you typed>>
Configured ethernet card(s) found in your system: wlan0 eth0
------------------------------------------------------
The public IP address of this server is NOT found.
Which ethernet port in this server is for public Internet accsess, not for DRBL connection?
Available ethernet ports in this server:
wlan0 (192.168.1.168), eth0 (192.168.99.1), 
[wlan0] <<choose the wireless card or network device that goes to the internet>>
The ethernet port you choose for the WAN connection: wlan0
The ethernet port(s) for DRBL environment:  eth0
Now we can collect the MAC address of clients! 

Do you want to collect them?
[y/N] N

Do you want to let the DHCP service in DRBL server offer same IP address to the client every time when client boots (If you want this function, you have to collect the MAC addresses of clients, and save them in file(s) (as in the previous procedure)). This is for the clients connected to DRBL server's ethernet network interface eth0 ?
[y/N] N
What is the initial number do you want to use in the last set of digits in the IP (i.e. the initial value of d in the IP address a.b.c.d) for DRBL clients connected to this ethernet port eth0.
[1] 100

How many DRBL clients (PC for students) connected to DRBL server's ethernet network interface enp0s8 ?
Please enter the number: 
[12] 20 <<max number of clients you will simultaneously clone>>

We will set the IP address for the clients connected to DRBL server's ethernet network interface enp0s8 as: 192.168.99.100-119
Accept ? [Y/n] Y
******************************************************
OK! Let's continue...
******************************************************
The Layout for your DRBL environment: 
******************************************************
          NIC    NIC IP                    Clients
+-----------------------------+
|         DRBL SERVER         |
|                             |
|+-- [wlan0] 192.168.1.168 +- to WAN
|                             |
|+-- [eth0] 192.168.99.1 +--- to clients group eth0 [ 20 clients, IPs 
|                             |               from 192.168.99.100-119]
+-----------------------------+
******************************************************
Total clients: 20
******************************************************
Press Enter to continue... <Enter>
******************************************************
------------------------------------------------------
In the system, there are 3 modes for diskless linux services:

Which mode do you prefer?
[0] 0
In the system, there are 4 modes available for clonezilla:

Which mode do you prefer?
[0] 0

When using clonezilla, which directory in this server you want to store the saved image (Please use absolute path, and do NOT assign it under /mnt/, /media/ or /tmp/)?
[/home/partimag] /home/partimag

do you want to use that swap partition or create a swap file in  the writable filesystem so that client has more memory to use? (This step will NOT destroy any data in that harddisk)
[Y/n] Y

What's the maximum size (Megabytes) for the swap space?
We will try to allocate the swap space for you, if it's not enough, 60% of the free space will be used.
[128] 128

Which mode do you want the clients to use after they boot?
"1": Graphic mode (X window system) (default),
"2": Text mode.
[1] 1

Which mode do you want when client boots in graphic mode?
0: normal login, 1: auto login, 2: timed login
[0] 0

Do you want to set the root's password for clients instead of using same root's password copied from server? (For better security)
[y/N] N

Do you want to set the pxelinux password for clients so that when client boots, a password must be entered to startup (For better security)
[y/N] N

Do you want to set the boot prompt for clients?
[Y/n] N
How many 1/10 sec is the boot prompt timeout for clients?
[70] 70

Do you want to use graphic background for PXE menu when client boots?
Note! If you use graphical PXELinux menu, however client fails to boot, you can switch to text mode by running "switch-pxe-bg-mode -m text".
[Y/n] Y

Do you want to let audio, cdrom, floppy, video and plugdev (like USB device) open to all users in the DRBL client? If yes, we will add all the users to those device groups in the server and client.
[Y/n] Y

Do you want to setup public IP for clients?
[y/N] N
------------------------------------------------------

Do you want to let client has an option to run terminal mode?
[y/N] N

Do you want to let DRBL server as a NAT server? If not, your DRBL client will NOT be able to access Internet.
[Y/n] Y

