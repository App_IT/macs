## App Academy Mass Cloning System (MaCS) Usage Guide:

This is a guide explaining how to create and restore images using the App Academy Mass Cloning System.  For details of how to set up and install DRBL and App Academy�s DRBL Web UI, refer to the Installation Guide.

On the Server, open Firefox and navigate to 127.0.0.1/drbl_gui or localhost/drbl_gui (if necessary)

##### Creating (saving) an image:

1. Prepare a �Master Computer� to be imaged.
2. Turn off the Master and plug connect via Ethernet to the network switch or directly into the cloning server.
3. Ensure the Master Computer and Cloning Server have power (if it is a laptop)
4. On the Cloning Server, at the top of the screen, type a descriptive name for the image (i.e. 9th-Grade).
5. On the Cloning Server, Click �Save Image�
6. On the Master Computer, turn the power on and repeatedly hit the key to get to the Boot Menu
a. Usually F12, DEL or F10.
7. On the Master Computer, choose �PXE� or �Network Boot�
8. On the Master Computer, a DRBL screen should appear, wait for a few seconds or press �enter�
9. The Master Computer will turn off or reboot when the process is complete.
 
##### Restoring an image:

1. Unplug the Master Computer from the switch or Cloning Server, if it is connected.
2. On the Cloning Server, If you�ve just created a new image, refresh the page with �F5� or by clicking the App Academy logo
3. Ensure computers to be imaged have power (if they are laptops)
4. Plug every Client Computer via Ethernet to the Network Switch
5. Turn on the �client computers� (those to be imaged,) and repeatedly hit a key to bring up the Boot Menu
  * Usually F12, DEL, or F10
6. Highlight �PXE� or �Network Boot�, but DO NOT PRESS ENTER YET
7. On the switch, check that lights are on for every connected computer
  * If your switch supports gigabit, the lights may indicate whether the connection is gigabit or 100Mb.  Ensure all connections are gigabit for the fastest cloning.
  * At 100Mb, the restore process can be VERY SLOW, For example, restoring a 120GB image to 8 computers takes about 45 minutes with a gigabit connection, or 4 to 6 hours at 100Mb.*
  * If you�re not getting a gigabit connection despite your hardware supporting it, try snugging the Ethernet cable at both ends, unplug and replug.*
8. On the Cloning Server, toward the bottom of the screen, select the number of clients (computers to be restored) and select from the list the image you�d like to restore to the Client Computers.
9. Click �Restore�
10. Wait for the screen to show the words �SUCCESS�
11. Now press ENTER on all the Client Computers.
12. A DRBL screen should appear on the Client Computers, wait for a few seconds or press �enter.�  The cloning process should begin once all machines reach a blue screen with a gray box.
13. Once finished the �client� computer will reboot.
