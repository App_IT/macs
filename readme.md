## Synopsis

The MaCS DRBL Server + DRBL WebUI is a solution for the mass cloning of computers.

A “master” computer is prepared, and an image is saved to the cloning server.  After that, the image can be restored to several client computers simultaneously.

## Overview & Design

The overall design schema looks like this:

##### Network Design Schema

![network_design](images/drbl_network_design.jpg)

## Caution!
Do not plug the cloning server or switch into your local network via Ethernet.  If a machine reboots anywhere on your local network while the cloning server is connected, DRBL may initiate the cloning process and erase all the machine’s data!
It’s possible to use a machine with two Ethernet adapters to avoid the above-mentioned scenario, but it’s easiest to use a Laptop or Desktop with a wireless adapter (for internet) and a gigabit Ethernet adapter (for the cloning LAN).  PXE booting (and therefore DRBL cloning) does not work over the WiFi, so using WiFi to connect to your local network (and/or internet) is safest.
 
## Installation
1. Install Ubuntu Desktop 14.04 (should work on 14 or later)
2. Find and note the names for Ethernet and wireless adapters: `$ ifconfig`
    * The Ethernet adapter will probably start with ‘e’ and look like `eth0` or `ensp01`
    * The WiFi adapter name will probably start with ‘w’ look like `wlan0` or `wlsp01`
    * The guide will assume eth0 for the Ethernet adapter and wlan0 for the wireless adapter.
    * Use the adapter names you found via ifconfig
3. (Optional) uninstall network-manager
    * `$ sudo apt-get remove network-manager`
4. Set a static IP for the Ethernet adapter.
    * If you removed network-manager, set it in /etc/network/interfaces similar to:
    * `$ sudo nano /etc/network/interfaces`
    * >auto eth0
    * >iface eth0 inet static
    * >address 192.168.99.1
    * >netmask 255.255.255.0

5. Connect the WiFi to the internet (if you removed network-manager, add wlan info to /etc/network/interfaces)
6. Install LAMP server. (Note: Use any password for msyql root.  MySQL is not used in this system.)
    * `$ sudo apt-get install lamp-server^`
7. Clone DRBL_GUI into /var/www/html/ 
    1. `$ cd /var/www/html/`
    2. `$ sudo git clone http://bitbucket.org/App_IT/macs`
8. Run the setup script (as root): `$ sudo /var/www/html/macs/setup/setup.sh`
9. Follow the prompts to set up DRBL according to your network and needs.
    * Usually, the default options are fine.  Read the options and if in doubt, choose the default.
    * You can refer to the [DRBL Options Guide](https://bitbucket.org/App_IT/macs/src/519f2eb1ddada29337f138be18c33b2cc897ae4e/docs/drbl_options_guide.md?at=master) for options to choose to mimic App Academy’s setup.

## Testing
1. Upon success, try “localhost/macs” or “127.0.0.1/macs” in a browser (recommend: Firefox)

2. Test “Save an Image”
    1. Type a name next to “Image Name”
    2. Click “Save Image”
    3. Check that a script is run and eventually reports “SUCCESS”
3. Reboot the server `$ sudo reboot`
4. Read [Usage Guide](https://bitbucket.org/App_IT/macs/src/906cb187ee68a0c20f48bf67a6373a0386d894d1/docs/MaCS_usage_guide.md?at=master) for usage

## License
Open Source / MIT License