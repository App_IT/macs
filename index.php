<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>DRBL WebUI</title>
</head>

<body>

<?php

    $image_directory = "/home/partimag";
    $maxToClone = "99";
    
    if (file_exists($image_directory)) {
        $dir_exists = true;
    } else {
        $dir_exists = false;
    }

    foreach (glob($image_directory . '/*') as $clonedImage) {
          $availableImages[] = $clonedImage;
    }
?>
        <br />
        <div class="wrapper">
            <div id ="banner">
                <a href="index.php"><img src="logo.png" class="centered" /></a>
                <h2 class="centered">App Academy DRBL WebUI</h2>
            </div> <!-- banner -->
            
            <div class="content">
                <div id="imageSave" class="actionBox">
                    <form action="save-image.php" method="post">
                        <fieldset>
                            <legend>Save an Image:</legend>
                            <label>Image Name:</label>
                            <input type="text" name="imageName" autocomplete="off" />
                            <button>Save Image</button>
                            <p class="smallText centered">(Spaces will be replaced with underscores)</p>
                        </fieldset>    
                    </form>    
                </div> <!-- imageSave -->
                <br />
                
                <div id="imageRestore" class="actionBox">
                    <form action="restore-image.php" method="post">
                        <fieldset>
                            <legend>Restore an Image</legend>
                            <?php
                                if ($dir_exists && isset($availableImages)) {
                                    
                                    echo '<table><tr><td>';
                                    
                                    echo '<label>Number to Clone:</label>';
                                    echo '<select name="numberToClone">';
                                        for ($i=1;$maxToClone>=$i;$i++) {
                                            echo '<option value= "' . $i . '">' . $i . '</option>';
                                            }
                                    echo '</option>';
                                    echo '</td>';
                                    echo '<td>';
                                    
                                    echo '<label>Image to Restore: </label>';
                                    echo '<select name="imageToClone">';
                                    foreach ($availableImages as $availableImage) {
                                        preg_match('/[^\/:*?"<>]+$/', $availableImage, $image_filename_match);
                                        $imageName = $image_filename_match[0];
                                        echo '<option value="' . $imageName . '">' . $imageName . "</option>";
                                        }
                                        echo '</select>';
                                    
                                    echo '</td></tr>';
                                    echo '</table>';
                                    echo '<div id="restoreButton" class="centered">';
                                        echo '<br /><br />';
                                        echo '<button>Restore Image</button>';
                                    echo '</div>';
                                } else {            
                                    echo '<h3>No Images Available for Restore</h3>';
                                        if (!$dir_exists) {
                                            echo '<p>The directory "' . $image_directory .'" does not seem to exist</p>';
                                        } else {
                                            echo '<p>The directory "' . $image_directory . '" exists but it appears no images are yet saved.</p>';
                                        }
                                    echo '<p>Restore function disabled until a saved image is available</p>';
                                    echo '<a href="javascript:window.location.reload();">Click here to check again</a>';
                                }
                            ?>
                        </fieldset>
                    </form>
                </div> <!-- imageRestore -->
            </div> <!-- content -->
        </div> <!-- wrapper -->
    </body>    
</html>
